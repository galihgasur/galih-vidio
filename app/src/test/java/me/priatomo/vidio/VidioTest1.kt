package me.priatomo.vidio

import org.junit.Test

class VidioTest1 {
    @Test
    fun main () {
        // print binary square 7 x 7
        printBinarySquare(7)
    }

    /*
    * Desc : a function to print binary square
    * Params : n is the dimension of the square with n more than 0
    * Output : printed binary square
    * */
    fun printBinarySquare (n: Int) {
        if (n < 0) {
            // don't print if n < 0
            return
        }

        for (i in 0 until n) {
            for (j in 0 until n) {
                val binary = if ((i + j) % 2 == 0) {
                    0
                } else {
                    1
                }
                // print binary horizontally
                print(binary)
            }
            // print enter
            println()
        }
    }
}