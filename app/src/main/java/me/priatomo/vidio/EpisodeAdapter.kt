package me.priatomo.vidio

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import me.priatomo.vidio.databinding.ItemEpisodeBinding

// pseudoclass for View Binding class
// class ItemEpisodeBinding(val titleView: TextView, val downloadButton: Button, val progressView: ProgressBar, val lytImage: ImageView)

class EpisodeViewHolder(val binding: ItemEpisodeBinding): RecyclerView.ViewHolder(binding.root){
    fun bind(episode: Episode, context: Context, vidioSDKHelper: VidioSDKHelper){
        binding.titleView.text = episode.title

        binding.downloadButton.isVisible = (episode.downloadStatus == DownloadStatus.NOT_DOWNLOADED)
        binding.progressBar.isVisible = (episode.downloadStatus == DownloadStatus.DOWNLOADING)

        binding.downloadButton.setOnClickListener {
            /***
             * TODO :
             * 1. call vidioSDK : VidioSDK.downloadEpisode(episodeId: Long) in another thread so it won't block UI.
             * 2. show progress bar when downloading.
             * 3. hide download button and progress bar when finish downloading.
             * 4. otherwise, show the download button.
             * 5. when error occurs, show the download button.
             *
             * Notes :
             * - You can use DownloadStatus enum below to help you update the UI correctly.
             * - Be carefull on accessing view in another thread, since recycleview can be recycled.
             */

            // create SDK Helper to make async task
            vidioSDKHelper.downloadEpisode(episode.id)
        }

        Glide.with(context)
            .load(episode.thumbnailUrl)
            .into(binding.lytImage)
    }
}

class EpisodeAdapter(private val episodes: List<Episode>, private val context: Context, private val vidioSDKHelper: VidioSDKHelper): RecyclerView.Adapter<EpisodeViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EpisodeViewHolder {
        return EpisodeViewHolder(ItemEpisodeBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: EpisodeViewHolder, position: Int) {
        holder.bind(episodes[position], context, vidioSDKHelper)
    }

    override fun getItemCount(): Int = episodes.size
}

data class Episode(
    val id: Long,
    val title: String,
    val duration: String,
    val thumbnailUrl: String,
    val description: String,
    val isFree: Boolean,
    var downloadStatus: DownloadStatus = DownloadStatus.NOT_DOWNLOADED
)

enum class DownloadStatus {
    NOT_DOWNLOADED,
    DOWNLOADING,
    DOWNLOAD_FINISH,
    DOWNLOAD_FAILED
}