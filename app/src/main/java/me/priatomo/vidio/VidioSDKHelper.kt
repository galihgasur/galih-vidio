package me.priatomo.vidio

import android.os.Handler
import android.os.Looper
import java.lang.Exception
import java.util.concurrent.Callable
import java.util.concurrent.Executor
import java.util.concurrent.Executors

// VidioSDKHelper, to make async task
class VidioSDKHelper : TaskRunner.Callback<DownloadResult> {
    var callback: Callback? = null

    fun downloadEpisode (episodeId: Long) {
        callback?.onStartDownload(episodeId)
        val runner = TaskRunner()
        runner.executeAsync(DownloadEpisodeTask(episodeId), this)
    }

    interface Callback {
        fun onFinishDownload(episodeId: Long, status: DownloadStatus)
        fun onStartDownload(episodeId: Long)
    }

    override fun onComplete(result: DownloadResult) {
        callback?.onFinishDownload(result.id, result.status)
    }
}

class TaskRunner {
    private val executor: Executor =
        Executors.newSingleThreadExecutor() // change according to your requirements
    private val handler = Handler(Looper.getMainLooper())

    interface Callback<R> {
        fun onComplete(result: R)
    }

    fun <R> executeAsync(callable: Callable<R>, callback: Callback<R>) {
        executor.execute {
            val result: R = callable.call()
            handler.post { callback.onComplete(result) }
        }
    }
}

data class DownloadResult (val id: Long, var status: DownloadStatus)

internal class DownloadEpisodeTask(private val episodeId: Long) :
    Callable<DownloadResult> {
    override fun call(): DownloadResult {
        return try {
            VidioSDK.downloadEpisode(episodeId)
            DownloadResult(episodeId, DownloadStatus.DOWNLOAD_FINISH)
        } catch (e : Exception) {
            DownloadResult(episodeId, DownloadStatus.DOWNLOAD_FAILED)
        }
    }
}
