package me.priatomo.vidio

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import me.priatomo.vidio.databinding.VidioActivityMainBinding

class MainActivity : AppCompatActivity(), VidioSDKHelper.Callback {
    val episodeList : ArrayList<Episode> = ArrayList()
    private lateinit var binding: VidioActivityMainBinding
    private val vidioSDKHelper : VidioSDKHelper = VidioSDKHelper()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = VidioActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // set vidio sdk helper callback
        vidioSDKHelper.callback = this

        // dummy data
        for (i in 0L until 50L) {
            episodeList.add(
                Episode(
                    i,
                    "Title $i",
                    "Duration ${i}0 menit",
                    "https://static-kmkonline.akamaized.net/assets/vidio/vidio-hero-logo-light-e060a7c1cd5914bef67f11775822b3b52b4ef52dd3cea211b0385f4b84ad2db2.png",
                    "Deskripsi $i",
                    i % 2L == 0L
                ))
        }

        // setup recycler view
        binding.episodeRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        val adapter = EpisodeAdapter(episodeList, this, vidioSDKHelper)
        binding.episodeRecyclerView.adapter = adapter
    }

    override fun onStartDownload(episodeId: Long) {
        findEpisodeIdx(episodeId)?.let {
            episodeList[it].downloadStatus = DownloadStatus.DOWNLOADING
            runOnUiThread {
                binding.episodeRecyclerView.adapter?.notifyItemChanged(it)
            }
        }
    }

    override fun onFinishDownload(episodeId: Long, status: DownloadStatus) {
        findEpisodeIdx(episodeId)?.let {
            episodeList[it].downloadStatus = status
            runOnUiThread {
                binding.episodeRecyclerView.adapter?.notifyItemChanged(it)
            }
        }
    }

    // get index of episode from episodeId, so adapter can notify data change
    fun findEpisodeIdx (episodeId: Long) : Int? {
        for ((i, episode) in episodeList.withIndex()) {
            if (episode.id == episodeId) {
                return i
            }
        }
        return null
    }
}